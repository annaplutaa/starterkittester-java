package egzamin;

public class TemperatureOutOfRange extends RuntimeException {
	
	public TemperatureOutOfRange(String message) {
		super(message);
	}
	
}
