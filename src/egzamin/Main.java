package egzamin;

import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {
		
		// creating objects
		
		Beko beko = new Beko();
		Whirlpool whirlpool = new Whirlpool();
		Amica amica = new Amica();
		
		// 1. Klasa pralka przechowuje numer aktualnego programu prania. Numer programu to liczba z zakresu 1-20.
		
		System.out.println("Z.1");
		System.out.println("Stworzenie klasy WashingMachine + ustalenie zakresu programu w WashingMachine.setProgramNumber \n");
		
		// 2. Pralka implementuje metody setProgram(int ) i getProgram() oraz metody nextProgram(), previusProgram().
		// Metoda set program ustawia numer programu getProgram zwraca numer programu. NextProgram zmienia na kolejny
		// (tak jak okrągłe pokrętło w pralce) i previusProgram poprzedni (w sensie jak jest 3 to ustawia 2).
		
		System.out.println("Z.2");
		System.out.println("Zmiana programów w pralkach: ");
		beko.setProgramNumber(7);
		System.out.println("wynik getProgramNumber: " + (beko.getProgramNumber()));
		System.out.println("wynik nextProgram: " + (beko.nextProgram()));
		System.out.println("wynik previusProgram: " + (beko.previusProgram() + "\n"));
		beko.setProgramNumber(20);
		System.out.println("wynik getProgramNumber: " + (beko.getProgramNumber()));
		System.out.println("wynik nextProgram: " + (beko.nextProgram()));
		System.out.println("wynik previusProgram: " + (beko.previusProgram() + "\n"));
		
		// 3. Pralka posiada zmienną temp która przechowuje wartość temperatury jako liczbę zmiennoprzecinkową ze
		// skokiem 0,5 stopnia. Temperatura jest z zakresu 0 – 90 stopni. (czyli np. 50,5).
		
		System.out.println("Z.3");
		System.out.println("Zmiana temperatury:");
		System.out.println("wynik getTemp: " + (whirlpool.getTemp()));
		System.out.println("tempUp: " + whirlpool.tempUp());
		System.out.println("tempDown: " + whirlpool.tempDown() + "\n");
		
		// 4. Pralka implementuje metody setTemp getTemp. Zwróć uwagę na metodę setTemp. Jeśli podamy mu liczbę 45,7
		// powinna ustawić temperaturę 45,5. 34,9 to 35.0 i tak dalej.
		
		System.out.println("Z.4");
		System.out.println("Zaokrąglanie temperatury:");
		amica.setTemp(60.68);
		System.out.println("getTemp: " + (amica.getTemp()) + "\n");
		
		// 5. Dopisz metody tempUp() i tempDown() (skok o 0,5 stopnia celsujasz). Gdy temp dojdzie do zera to nie da się
		// już zmniejszyc temperatury. Tak samo gdy jest temperatura 90 metoda tempUp nie ustawia już większej
		// temperatury. Przy próbie zmiany temperatury poza zakres powinien się pojawić błąd o przekroczonym zakresie.
		
		System.out.println("Z.5");
		System.out.println("Zmiana temperatury o 0.5 stopnia:");
		beko.setTemp(90);
		// System.out.println("tempUpException: " + beko.tempUp());
		System.out.println("wynik tempDown: " + beko.tempDown() + "\n");
		amica.setTemp(54.44);
		System.out.println("wynik tempUp: " + amica.tempUp());
		System.out.println("wynik tempDown: " + amica.tempDown() + "\n");
		
		System.out.println("Łapanie wyjątków:");
		whirlpool.setTemp(33);
		System.out.println("Wysokość aktualnej temperatury: " + (whirlpool.getTemp()));
		
		try {
			whirlpool.setTemp(100);
		} catch (TemperatureOutOfRange e) {
			e.printStackTrace();
		}
		
		System.out.println("Temperatura po złapaniu wyjątku: " + (whirlpool.getTemp() + "\n"));
		
		// 6. Zmiana temperatury powoduje wyświetlenie się komunikatu o temperaturze np. „current temp 75 ºC”.
		
		System.out.println("Z.6");
		System.out.println("Wyświetlanie komunikatu o temperaturze:");
		whirlpool.setTemp(55.87);
		System.out.println("wynik tempUp: " + whirlpool.tempUp());
		System.out.println("wynik tempDown: " + whirlpool.tempDown() + "\n");
		
		// 7. Pralka przechowuje wartość 0 – 1000 predkości wirowania. Metody setV getV i upV i downV. Skok obrotów o
		// 100. Przy czym gdy naciśniemy down przy 0 wartośc ma się zmienic na 1000, a gdy wartość jest 1000 i
		// naciśniemy upV to 0.
		
		System.out.println("Z.7");
		System.out.println("Działanie zmiany prędkości wirowania:");
		amica.setV(1000);
		System.out.println("wynik getV: " + (amica.getV()));
		System.out.println("wynik upV: " + (amica.upV()));
		System.out.println("wynik downV: " + (amica.downV()) + "\n");
		whirlpool.setV(588);
		System.out.println("wynik getV: " + (whirlpool.getV()));
		System.out.println("wynik upV: " + (whirlpool.upV()));
		System.out.println("wynik downV: " + (whirlpool.downV()) + "\n");
		
		// 8. Pralka posiada funkcje showStatus która wypisuje komunikat złożony z wszytskich wartości które
		// przechowuje pralka. Np.: numer programu 4, temperatura 60 ºC
		
		System.out.println("Z.8");
		System.out.println("Funkcja showStatus:");
		beko.showStatus();
		whirlpool.showStatus();
		amica.showStatus();
		
		// 9. Stwórz klasy pralek Beko, Whirlpool, Amica. Pralki te zachowują się tak samo jak pralka z małymi
		// zmianami. Każda z pralek ma atrybut z nazwą marki. Nazwy należą do słownika nazw.
		
		System.out.println("\n" + "Z.9");
		System.out.println("Stworzenie klas Beko, Whirlpool, Amica + dodanie do nich pola name \n");
		
		// 10. Pralka beko ma skok temperatury nie o 0,5 stopnia tylko o 1.
		
		System.out.println("Z.10");
		beko.setTemp(70);
		System.out.println("wynik getTemp: " + (beko.getTemp()));
		System.out.println("wynik tempUp: " + beko.tempUp());
		System.out.println("tempDown: " + beko.tempDown() + "\n");
		
		// 11. Pralka whirlpool ma 25 programów prania.
		
		System.out.println("Z.11");
		System.out.println("Zmiana programów w pralce Whirlpool:");
		whirlpool.setProgramNumber(25);
		System.out.println("wynik getProgramNumber: " + (whirlpool.getProgramNumber()));
		System.out.println("wynik nextProgram: " + (whirlpool.nextProgram()));
		System.out.println("wynik previusProgram: " + (whirlpool.previusProgram() + "\n"));
		
		// 12. Stwórz klasę Main, która zaprezentuje każdy z powyższych podpunktow.
		
		System.out.println("Z.12");
		System.out.println("Dodane metody do sprawdzenia działania programu \n");
		
		// 13. Stwórz listę, do której dodasz po jednej pralce z każdej firmy. Napisz funkcję, która posortuje
		// alfabetycznie pralki po nazwie i wypisze liste przed i po sortowaniu. Do tej operacji postaraj się
		// użyć bibiloteki stream.
		
		System.out.println("Z.13");
		
		List<WashingMachine> washingMachines = new ArrayList<>();
		
		washingMachines.add(beko);
		washingMachines.add(whirlpool);
		washingMachines.add(amica);
		
		System.out.println("Przed sortowaniem:");
		
		washingMachines.stream()
				.forEach(System.out::println);
		
		System.out.println('\n' + "Po sortowaniu:");
		washingMachines.stream()
				// .peek(System.out::println)
				.sorted((a, b) -> a.getName()
						.compareTo(b.getName()))
				.forEach(System.out::println);
		
	}
	
}
