package egzamin;

public abstract class WashingMachine {
	
	// fields of WashingMachine
	
	protected int					programNumber	= 1;
	protected double				temp			= 0;
	protected int					v				= 0;
	protected WashingMachineName	name;
	
	// variables min, max
	
	protected int			maxProgram	= 20;
	private final int		minProgram	= 1;
	protected final double	maxTemp		= 90;
	protected final double	minTemp		= 0;
	private final int		maxV		= 1000;
	private final int		minV		= 0;
	
	static final String DEGREE = "\u00b0"; // znaczek stopnie Celsjusza
	
	// methods of program
	
	public int nextProgram() {
		programNumber += 1;
		
		if (programNumber > maxProgram) {
			programNumber = minProgram;
		}
		return programNumber;
	}
	
	public int previusProgram() {
		programNumber -= 1;
		
		if (programNumber < minProgram) {
			programNumber = maxProgram;
		}
		return programNumber;
	}
	
	// methods of temperature
	
	public double tempUp() {
		temp += 0.5;
		
		if (temp > maxTemp) {
			throw new TemperatureOutOfRange("Nie ma takiej temperatury");
		}
		System.out.println("current temperature: " + this.temp + " " + DEGREE + "C");
		return temp;
	}
	
	public double tempDown() {
		temp -= 0.5;
		
		if (temp < minTemp) {
			throw new TemperatureOutOfRange("Nie ma takiej temperatury");
		}
		System.out.println("current temperature: " + this.temp + " " + DEGREE + "C");
		return temp;
		
	}
	
	public double roundTemp(double temp) {
		double mainTemp = (int) Math.floor(temp);
		double newTemperature = temp * 100;
		int differenceTemp = (int) newTemperature - (int) mainTemp * 100;
		
		if (differenceTemp <= 24) {
			mainTemp += 0;
		} else if (differenceTemp >= 75) {
			mainTemp += 1;
		} else {
			mainTemp += 0.5;
		}
		return mainTemp;
	}
	
	// methods of spin speed (v)
	
	public int upV() {
		v += 100;
		
		if (v > maxV) {
			v = minV;
		}
		return v;
	}
	
	public int downV() {
		v -= 100;
		
		if (v < minV) {
			v = maxV;
		}
		return v;
	}
	
	// other methods
	
	public void showStatus() {
		System.out.println("nazwa: " + this.name + ", program: " + this.programNumber + ", temperature: " + this.temp + DEGREE + "C" + ", prędkość wirowania: " + this.v);
	}
	
	@Override
	public String toString() {
		return "nazwa: " + this.name + ", program: " + this.programNumber + ", temperatura: " + this.temp + DEGREE + "C" + ", prędkość wirowania: " + this.v;
	}
	
	public String getName() {
		return name.toString();
	}
	
	// getters & setters
	
	public void setProgramNumber(int programNumber) {
		
		if (programNumber < minProgram || programNumber > maxProgram) {
			throw new ProgramOutOfRange("Nie ma takiego programu");
		} else {
			this.programNumber = programNumber;
		}
	}
	
	public int getProgramNumber() {
		return programNumber;
	}
	
	public void setTemp(double temp) {
		if (temp < minTemp || temp > maxTemp)
			throw new TemperatureOutOfRange("Nie ma takiej temperatury");
		this.temp = roundTemp(temp);
		System.out.println("current temperature: " + this.temp + " " + DEGREE + "C");
	}
	
	public double getTemp() {
		return temp;
	}
	
	public void setV(int v) {
		
		if (v < minV || v > maxV) {
			throw new VOutOfRange("Nie ma takiej prędkości wirowania");
		} else if (v % 100 == 0) {
			this.v = v;
		} else {
			this.v = (int) Math.round((double) v / 100) * 100;
		}
	}
	
	public int getV() {
		return v;
	}
	
	public void setName(WashingMachineName name) {
		this.name = name;
	}
	
}
