package egzamin;

public class ProgramOutOfRange extends RuntimeException {
	
	public ProgramOutOfRange(String message) {
		super(message);
	}
	
}
