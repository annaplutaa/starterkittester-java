package egzamin;

public class Beko extends WashingMachine {
	
	// constructor
	
	public Beko() {
		this.name = WashingMachineName.BEKO;
	}
	
	// methods
	
	@Override
	public double tempUp() {
		temp += 1;
		
		if (temp > maxTemp) {
			throw new IndexOutOfBoundsException("Nie ma takiej temperatury");
		}
		System.out.println("current temperature: " + temp + " " + DEGREE + "C");
		return temp;
	}
	
	@Override
	public double tempDown() {
		this.temp -= 1;
		
		if (temp < minTemp) {
			throw new IndexOutOfBoundsException("Nie ma takiej temperatury");
		}
		System.out.println("current temperature: " + temp + " " + DEGREE + "C");
		return temp;
	}
	
	@Override
	public double roundTemp(double temp) {
		double mainTemp = (int) Math.round(temp);
		return mainTemp;
	}
	
}
