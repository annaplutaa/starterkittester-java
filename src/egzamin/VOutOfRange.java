package egzamin;

public class VOutOfRange extends RuntimeException {
	
	public VOutOfRange(String message) {
		super(message);
	}
	
}
